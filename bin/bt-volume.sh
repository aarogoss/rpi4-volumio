#!/bin/bash

while true;
do
    # get the headphones volume
    vol=$(amixer sget -c 1 SoftMaster | sed -nre 's/.*Left: ([0-9]+) .*/\1/p')
    amixer sset -M '## REPLACE WITH BT LABEL FROM HCITOOL ## - A2DP' ${vol}%
    sleep 1
done
