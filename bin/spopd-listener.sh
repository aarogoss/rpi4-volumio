#!/bin/bash -x

while true;
do
    # check to ensure spop is running, if not restart
    echo "Checking spop is running..."
    systemctl status spop
    if [[ $? != 0 ]]; then
        echo "Spop is stopped.  Restarting..."
        sudo systemctl start spop
        echo "Spop restarted"
    else
        echo "Spop is running..."
    fi
    echo "Sleeping 5 seconds before next spop check..."
    sleep 5
done
