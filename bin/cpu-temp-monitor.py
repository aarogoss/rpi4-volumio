#!/usr/bin/env python3
import re
import subprocess
import sys
import time

import RPi.GPIO as gpio

TRIGGER_PIN = 14

TEMP_REG = re.compile(r"^temp=([0-9]+)[^0-9].*$")
TEMP_UPPER_THRESHOLD = 50  # degrees C
TEMP_LOWER_THRESHOLD = 40  # degrees C

TEMP_UPPER_DURATION = 10  # seconds
TEMP_LOWER_DURATION = 120  # seconds

def temperature():
    """Run the vcgen command and parse the results into an int

    :return:
    """
    try:
        output = subprocess.check_output(["sudo", "vcgencmd", "measure_temp"])
        print("Found output {}".format(output))
        match = TEMP_REG.match(output.decode("utf-8").strip())
        if match:
            return int(match.group(1))
    except subprocess.CalledProcessError:
        return -1


def change_fan(enabled=False):
    """Basic function: enable the trigger pin...or don't

    :param enable:
    :return:
    """
    print("Triggering hit: {}".format(enabled))
    gpio.output(TRIGGER_PIN, enabled)


def main():
    upper_counter = 0
    lower_counter = 0
    while True:
        temp = temperature()
        if temp != -1 and temp >= TEMP_UPPER_THRESHOLD:
            if upper_counter >= TEMP_UPPER_DURATION:
                upper_counter = 0
                lower_counter = 0
                change_fan(enabled=True)
            else:
                upper_counter += 2
                lower_counter -= 2
        if temp != -1 and temp <= TEMP_LOWER_THRESHOLD:
            if lower_counter >= TEMP_LOWER_DURATION:
                lower_counter = 0
                upper_counter = 0
                change_fan(enabled=False)
            else:
                upper_counter -= 2
                lower_counter += 2

        time.sleep(2)  # we can rest for 2 sec
    gpio.cleanup()


if __name__ == "__main__":
    gpio.setmode(gpio.BCM)
    gpio.setup(TRIGGER_PIN, gpio.OUT)
    sys.exit(main())
