#!/bin/bash
function die() {
    echo "$1"
    exit 1
}

function connected() {
    echo "Checking connection..."
    hcitool conn | grep ${DEV} &> /dev/null
    return $?
}

function scontrol_ready() {
    controls=$(amixer scontrols)
    if [[ $controls =~ ".*## REPLACE WITH BT NAME FROM HCITOOL ##.*" ]]; then
        return 0
    else
        return 1
    fi
}

DEV="## BT ID ##"

while true
do
    # if we are already connected, no problem
    connected && sleep 2 && continue

    # first, are the speakers even available?
    echo "Checking speakers are available..."
    avail=$(hcitool name ${DEV})

    [[ -z "${avail}" ]] && sleep 2 && continue

    # speakers are available, so let's connect
    echo "Speakers are available, attempting connection at 50% volume..."
    echo "connect ${DEV}" | bluetoothctl &> /dev/null
    countdown=10
    while [ $countdown -ge 0 ]
    do
        scontrol_ready && break
        echo "Waiting for connection..."
        countdown=$(( $countdown - 1 ))
        sleep 1
    done

    amixer sset '## REPLACE WITH BT NAME FROM HCITOOL ## - A2DP' 50%
    sleep 2
done

exit $?

